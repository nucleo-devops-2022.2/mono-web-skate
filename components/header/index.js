import Link from 'next/link';
import styles from './header.module.css';

export default function Header() {
  return (
    <header className={styles.headerContainer}>
      <h1 className={styles.title}>Skateshop</h1>
      <nav>
        <ul className={styles.listContainer}>
          <li className={styles.listItem}>
            <Link href="www.google.com">Web Repo</Link>
          </li>
            <li className={styles.listItem}>
            <Link href="www.google.com">Api Repo</Link>
          </li>
        </ul>
      </nav>
    </header>
  )
}
