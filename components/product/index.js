import styles from './product.module.css';

export default function Product({product}) {
  console.log(product);
  return (
    <div className={styles.container}>
      <div>
        <h3 className={styles.productName}>{product.name}</h3>
        <p className={styles.productDescription}>{product.description}</p>
        <p className={styles.productPrice}>{product.price} R$</p>
      </div>
      <div className={styles.buttonContainer}>
        <button>
          -
        </button>
        <button>
          +
        </button>
      </div>
    </div>
  )
}
