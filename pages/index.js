import { useState, useEffect } from 'react';
import Header from '../components/header';
  import Footer from '../components/footer';
  import Product from '../components/product';
  import styles from '../styles/index.module.css';
  import axios from 'axios';

  export default function Home() {
    const [data, setData] = useState([]);
    useEffect(() => {
      axios.get(`http://localhost:8080/product`).then(res => setData(res.data));
    }, [])

    
    return (
      <div>
        <Header/>
          <main className={styles.container}>
          {data.map(product => <Product key={product.id} product={product}/>)}
        </main>
      <Footer/>
    </div>
  )
}
